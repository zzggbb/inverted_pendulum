import numpy as np

def ddθ(G, m, M, L, U, θ, dθ):
  numer = (m+M)*G*np.tan(θ) - m*L*np.sin(θ)*dθ**2 - U
  denom = (m+M)*L/np.cos(θ) - m*L*np.cos(θ)
  return numer / denom

def ddx(G, L, θ, ddθ):
  numer = G*np.sin(θ) - L*ddθ
  denom = np.cos(θ)
  return numer/denom

def U(θ, dθ, x, dx):
  K = np.array([1100, 400, 110, 160])
  X = np.array([np.sin(θ), dθ, x, dx])
  return K.dot(X)
