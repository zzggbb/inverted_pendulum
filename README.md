## Inverted Pendulum on a Cart Simulator

![cover image](cover.png)

Graphical simulator of an inverted pendulum on a cart.
Made with Python and OpenGL.

---

### Using the simulator

Running the simulator:
```bash
$ python3 main.py
```

Keybindings:
| key combo | action |
|-:         |:-      |
| space     | Pause/play the simulation |
| q         | Quit the simulation |
| c         | Toggle the stabilization controller |
| r         | Reset all physical variables to their default value |
| T/t       | Increase/decrease θ of pendulum by 0.05 rad |
| L/l       | Increase/decrease length of pendulum by 0.1 |
| U/u       | Increase/decrease simulation updates per frame (UPF) by 5 |

---

### Simulator details

Physical Variables:
| name | default value   | units  |
| -    | -               | -      |
| Pendulum length | 0.3  | meters |
| Pendulum mass   | 0.1  | kg     |
| Cart mass       | 0.6  | kg     |
| Gravity         | 9.8  | m/s^2  |
| Initial theta   | 0.25 | rad    |

Simulation Variables:
| name | default value | units |
| -    | -             | -     |
| Updates per frame | 2000 |
