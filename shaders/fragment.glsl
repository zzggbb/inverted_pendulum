#version 330
uniform vec3 dimensions;
uniform vec4 track;    // ax ay bx by
uniform float pendulum_radius;
uniform vec2 cart_dimensions;
uniform vec2 pendulum_position;
uniform float cart_x;
uniform float input_force;
out vec4 color;

float sd_circle(vec2 p, float r) {
  return length(p) - r;
}

float sd_box(vec2 p, vec2 b) {
  vec2 d = abs(p) - b;
  return length(max(d, 0.0)) + min(max(d.x, d.y), 0.0);
}

float sd_line(vec2 p, vec2 a, vec2 b) {
  vec2 pa = p - a;
  vec2 ba = b - a;
  float h = clamp(dot(pa,ba)/dot(ba,ba), 0.0, 1.0);
  return length(pa - ba*h);
}

void main() {
  vec2 p = (2.0*gl_FragCoord.xy - dimensions.xy)/dimensions.y;
  vec2 cart_position = vec2(cart_x, 0.0);

  float d_pendulum = abs(sd_circle(p-pendulum_position, pendulum_radius));
  float d_cart = abs(sd_box(p-cart_position, cart_dimensions));
  float d_rod = sd_line(p, cart_position, pendulum_position);
  float d_track = sd_line(p, track.xy, track.zw);
  float main_model = min(min(d_pendulum, d_cart), min(d_rod, d_track));
  float main_curve = 1.0 - smoothstep(0.0, 0.005, main_model);

  vec2 force_arrow_tip = cart_position + vec2(input_force, 0.0);
  float d_force_arrow = sd_line(p, cart_position, cart_position + vec2(input_force, 0.0));
  float d_force_arrow1 = sd_line(p, force_arrow_tip, force_arrow_tip - input_force*0.25*vec2(1, 1));
  float d_force_arrow2 = sd_line(p, force_arrow_tip, force_arrow_tip - input_force*0.25*vec2(1, -1));
  float force_arrow_model = min(min(d_force_arrow1, d_force_arrow2), d_force_arrow);
  float force_arrow_curve = 1.0 - smoothstep(0.0, 0.005, force_arrow_model);

  vec3 c = vec3(main_curve) + vec3(force_arrow_curve, 0.0, 0.0);
  color = vec4(c, 1.0);
}
