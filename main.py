import numpy as np
import moderngl_window as mglw

from config import *
import model

CORNERS = np.array([-1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0], dtype='f4')

class InvertedPendulum(mglw.WindowConfig):
  gl_version = (3, 3)
  window_size = (1920, 1080)
  aspect_ratio = None
  title = "float_me"

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.prog = self.ctx.program(
      vertex_shader = open('shaders/vertex.glsl').read(),
      fragment_shader = open('shaders/fragment.glsl').read()
     )

    self.prog['pendulum_radius'] = PENDULUM_RADIUS
    self.prog['cart_dimensions'] = (CART_WIDTH, CART_HEIGHT)

    self.vbo = self.ctx.buffer(CORNERS)
    self.vao = self.ctx.vertex_array(self.prog, [(self.vbo, '2f', 'vertex')])

    # system variables
    self.reset_system()

    # simulation variables
    self.controller_enabled = True
    self.U = model.U(self.θ, self.dθ, self.x, self.dx)
    self.updates_per_frame = DEFAULT_UPDATES_PER_FRAME
    self.paused = True
    self.updates = 0
    self.update_shader_inputs(0)

  def reset_system(self):
    self.L = DEFAULT_PENDULUM_LENGTH
    self.θ = DEFAULT_THETA
    self.dθ = self.ddθ = self.x = self.dx = self.ddx = 0.0

  def integrate(self, dt):
    self.U = model.U(self.θ, self.dθ, self.x, self.dx) if self.controller_enabled else 0
    self.ddθ = model.ddθ(GRAVITY, PENDULUM_MASS, CART_MASS, self.L, self.U, self.θ, self.dθ)
    self.ddx = model.ddx(GRAVITY, self.L, self.θ, self.ddθ)
    self.dθ = self.dθ + dt*self.ddθ
    self.θ = self.θ + dt*self.dθ
    self.dx = self.dx + dt*self.ddx
    self.x = self.x + dt*self.dx
    self.updates += 1

  def integrate_period(self, render_delta):
    dt = render_delta / self.updates_per_frame
    for i in range(self.updates_per_frame):
      self.integrate(dt)

  def update_shader_inputs(self, dt):
    self.prog['pendulum_position'] = (self.x + np.sin(self.θ)*self.L, np.cos(self.θ)*self.L)
    self.prog['cart_x'] = self.x
    self.prog['input_force'] = model.U(self.θ, self.dθ, self.x, self.dx) if self.controller_enabled else 0

  def render(self, t, render_delta):
    if not self.paused: self.integrate_period(render_delta)
    self.update_shader_inputs(render_delta)
    self.vao.render(mode=self.ctx.TRIANGLE_FAN)
    print(f"θ={self.θ:.3f} θ'={self.dθ:.3f} x={self.x:.3f} x'={self.dx:.3f} U={self.U:.3f}")

  def resize(self, width, height):
    self.prog['dimensions'] = (width, height, width/height)
    self.prog['track'] = (-width/height, -0.03, width/height, -0.03)

  def key_event(self, key, action, modifiers):
    if action != self.wnd.keys.ACTION_PRESS: return
    shift = (1 if modifiers.shift else -1)
    if key == self.wnd.keys.SPACE: self.paused = not self.paused
    if key == self.wnd.keys.Q:     self.wnd.close()
    if key == self.wnd.keys.R:     self.reset_system()
    if key == self.wnd.keys.U:     self.updates_per_frame = np.clip(self.updates_per_frame+5*shift, 5, None)
    if key == self.wnd.keys.L:     self.L = np.clip(self.L + 0.1*shift, 0.1, None)
    if key == self.wnd.keys.T:     self.θ += 0.05*shift; self.dθ = 0; self.ddθ = 0
    if key == self.wnd.keys.C:     self.controller_enabled = not self.controller_enabled

if __name__ == '__main__':
  InvertedPendulum.run()
